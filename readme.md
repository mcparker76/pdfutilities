This is a wrapper utility for LibrePDF's openpdf library: https://github.com/LibrePDF/OpenPDF

Functionality includes easily creating tables, cells, paragraphs, and fonts.