/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.pdf_utilities;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

/**
 * Utility class for creating PDF files
 * 
 * @author Matt
 *
 */
public class PDFUtilities {

	private static final Font DEFAULT_BODY_FONT = PdfFonts.ARIAL_BODY;
	
	private PDFUtilities(){
		//no implementation
	}

	/**
	 * Create table
	 * @param columns - number of columns
	 * @param widthPercentage - percentage table takes of document
	 * @param widths - width of each column
	 * @return - the table
	 * @throws IllegalArgumentException
	 */
	public static PdfPTable createTable(int columns, int widthPercentage, int[] widths) {
		if (widthPercentage > 100) {
			throw new IllegalArgumentException("Width percentage is > 100");
		}
		
		int sum = 0;
		for (int width : widths) {
			sum += width;
		}
		
		if (sum > widthPercentage) {
			throw new IllegalArgumentException("Sum of widths exceeds " + widthPercentage);
		}
		
		PdfPTable table = new PdfPTable(columns);
		table.setWidthPercentage(widthPercentage);
		table.setWidths(widths);
		table.setHeaderRows(1);
		
		return table;
	}
	
	/**
	 * Create centered table cell
	 * @param text - the text
	 * @return - @PdfCell
	 */
	public static PdfPCell createCenteredTableCell(String text) {
		return createDefaultTableCell(text, Element.ALIGN_CENTER);
	}

	private static PdfPCell createDefaultTableCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, DEFAULT_BODY_FONT));
		cell.setHorizontalAlignment(alignment);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return cell;
	}
	
	/**
	 * Create centered table cell
	 * @param text - the text
	 * @param font - desired font
	 * @return @PdfCell
	 */
	public static PdfPCell createCenteredTableCell(String text, Font font) {
		return createDefaultTableCell(text, Element.ALIGN_CENTER, font);
	}

	/**
	 * Create left aligned table cell
	 * @param text - the text 
	 * @param font - desired font
	 * @return - @PdfCell
	 */
	public static PdfPCell createLeftAlignedTableCell(String text, Font font) {
		return createDefaultTableCell(text, Element.ALIGN_LEFT, font);
	}

	/**
	 * Create right aligned table cell
	 * @param text - the text 
	 * @param font - desired font
	 * @return - @PdfCell
	 */
	public static PdfPCell createRightAlignedTableCell(String text, Font font) {
		return createDefaultTableCell(text, Element.ALIGN_RIGHT, font);
	}

	/**
	 * Create default table cell
	 * @param text - the text
	 * @param alignment - alignment
	 * @param font - desired font
	 * @return - @PdfCell
	 */
	public static PdfPCell createDefaultTableCell(String text, int alignment, Font font) {
		PdfPCell cell = new PdfPCell(new Phrase(text, font));
		cell.setHorizontalAlignment(alignment);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return cell;
	}

	/**
	 * Create centeed paragraph with default font
	 * @param text - the text
	 * @return - @Paragraph
	 */
	public static Paragraph createCenteredParagraph(String text) {
		return createCenteredParagraph(text, DEFAULT_BODY_FONT);
	}
	
	/**
	 * Create left aligned paragraph with default font
	 * @param text - the text
	 * @return - @Paragraph
	 */
	public static Paragraph createLeftAlignedParagraph(String text) {
		return createLeftAlignedParagraph(text, DEFAULT_BODY_FONT);
	}
	
	/**
	 * Create centered paragraph
	 * @param text - the text
	 * @param font - desired @Font
	 * @return - @Paragraph
	 */
	public static Paragraph createCenteredParagraph(String text, Font font) {
		Paragraph paragraph = new Paragraph(text, font);
		paragraph.setAlignment(Element.ALIGN_CENTER);
		return paragraph;
	}
	
	/**
	 * Create left aligned paragraph
	 * @param text - the text
	 * @param font - desired @Font
	 * @return - @Paragraph
	 */
	public static Paragraph createLeftAlignedParagraph(String text, Font font) {
		Paragraph paragraph = new Paragraph(text, font);
		paragraph.setAlignment(Element.ALIGN_LEFT);
		return paragraph;
	}
}