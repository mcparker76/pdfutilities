/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.pdf_utilities;

import com.lowagie.text.Font;

/**
 * The supported PDF fonts
 * @author mcpar
 *
 */
public enum PdfFontFamily {
	
	COURIER("Courier", Font.COURIER),
	ARIAL("Arial", Font.HELVETICA),
	TIMES_ROMAN("Times Roman", Font.TIMES_ROMAN);
	
	private String name;
	private int fontFamily;
	
	private PdfFontFamily(String name, int fontFamily) {
		this.name = name;
		this.fontFamily = fontFamily;
	}
	
	/**
	 * Get the name (e.g. Times Roman)
	 * @return - name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get the font family (e.g. 2 is Times Roman)
	 * @return
	 */
	public int getFontFamily() {
		return fontFamily;
	}

	/**
	 * Get font family from name 
	 * @param name - the name
	 * @return - the font family
	 */
	public static PdfFontFamily getFontFamily(String name) {
		PdfFontFamily value = null;
		if (name.equalsIgnoreCase(PdfFontFamily.ARIAL.name)) {
			value = ARIAL;
		} else if (name.equalsIgnoreCase(PdfFontFamily.COURIER.name))  {
			value = COURIER;
		} else if (name.equalsIgnoreCase(PdfFontFamily.TIMES_ROMAN.name))  {
			value = TIMES_ROMAN;
		}
		
		if (value == null) {
			throw new IllegalArgumentException("Invalid font name: " + name);
		}
		
		return value;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}
}
