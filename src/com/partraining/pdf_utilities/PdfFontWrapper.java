/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
package com.partraining.pdf_utilities;

import com.lowagie.text.Font;

public class PdfFontWrapper {
	
	private final float size;
	private final boolean isBold;
	private final boolean isItalic;
	private final PdfFontFamily family;
	private final Font font;
	
	/**
	 * Constructor
	 * @param family - the font family
	 * @param size - the size
	 */
	public PdfFontWrapper(PdfFontFamily family, float size) {
		this(family, size, false, false);
	}
	
	/**
	 * Constructor 
	 * @param size - size
	 * @param isBold - true is bold
	 * @param family - font family
	 */
	private PdfFontWrapper(PdfFontFamily family, float size, boolean isBold, boolean isItalic) {
		this.size = size;
		this.isBold = isBold;
		this.isItalic = isItalic;
		this.family = family;
		
		if (isBold && isItalic) {
			font = new Font(family.getFontFamily(), size, Font.BOLDITALIC);
		} else if (isBold) {
			font = new Font(family.getFontFamily(), size, Font.BOLD);
		} else if (isItalic) {
			font = new Font(family.getFontFamily(), size, Font.ITALIC);
		} else {
		    font = new Font(family.getFontFamily(), size);
		}
	}
	
	/**
	 * Create a bold font
	 * @param family - the font family
	 * @param size - the size
	 * @return - the font
	 */
	public static PdfFontWrapper createBoldFont(PdfFontFamily family, float size) {
		return new PdfFontWrapper(family, size, true, false);
	}
	
	/**
	 * Create an italic font
	 * @param family - the font family 
	 * @param size - the size
	 * @return - the font
	 */
	public static PdfFontWrapper createItalicFont(PdfFontFamily family, float size) {
		return new PdfFontWrapper(family, size, false, true);
	}
	
	/**
	 * Create a bold, italic font
	 * @param family - the font family
	 * @param size - the size
	 * @return - the PdfFontWrapper
	 */
	public static PdfFontWrapper createBoldItalicFont(PdfFontFamily family, float size) {
		return new PdfFontWrapper(family, size, true, true);
	}
	
	/**
	 * Get the font
	 * @return - the com.lowagie.text.Font
	 */
	public Font getFont() {
		return font;
	}

	/**
	 * @return the size
	 */
	public float getSize() {
		return size;
	}

	/**
	 * @return the isBold
	 */
	public boolean isBold() {
		return isBold;
	}
	
	/**
	 * 
	 * @return the isItalic
	 */
	public boolean isItalic() {
		return isItalic;
	}

	/**
	 * @return the family
	 */
	public PdfFontFamily getFamily() {
		return family;
	}
	
	@Override
	public String toString() {
		return family.getName();
	}
	
}
