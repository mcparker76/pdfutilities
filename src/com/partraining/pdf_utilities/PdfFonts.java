/**
 * Copyright (c) 2021
 * Par Training Software Solutions
 * All rights reserved.
 * 
 * All intellectual and technical concepts contained herein are protected
 * by trade secret or copyright laws.
 * 
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from
 * Par Training Software Solutions.
 */
/**
 * 
 */
package com.partraining.pdf_utilities;

import com.lowagie.text.Font;

/**
 * @author mcpar
 *
 */
public final class PdfFonts {
	
	private static final int BODY = 8;
	private static final int H1 = 16;
	private static final int H2 = 14;
	private static final int H3 = 10;
	
	/**Arial, 8 */
	public static final Font ARIAL_BODY = new Font(Font.HELVETICA, BODY);
	/**Arial, 8 BOLD */
	public static final Font ARIAL_BOLD_BODY = new Font(Font.HELVETICA, BODY, Font.BOLD);
	/**Arial, 8 ITALIC */
	public static final Font ARIAL_ITALIC_BODY = new Font(Font.HELVETICA, BODY, Font.ITALIC);
	/**Arial, 16 */
	public static final Font ARIAL_H1 = new Font(Font.HELVETICA, H1);
	/**Arial, 14 */
	public static final Font ARIAL_H2 = new Font(Font.HELVETICA, H2);
	/**Arial, 10 */
	public static final Font ARIAL_H3 = new Font(Font.HELVETICA, H3);
	
	/**Times New Roman, 8 */
	public static final Font TIMES_NEW_ROMAN_BODY = new Font(Font.TIMES_ROMAN, BODY);
	/**Times New Roman, 8 BOLD */
	public static final Font TIMES_NEW_ROMAN_BOLD_BODY = new Font(Font.TIMES_ROMAN, BODY, Font.BOLD);
	/**Times New Roman, 8 ITALIC */
	public static final Font TIMES_NEW_ROMAN_ITALIC_BODY = new Font(Font.TIMES_ROMAN, BODY, Font.ITALIC);
	/**Times New Roman, 16 */
	public static final Font TIMES_NEW_ROMAN_H1 = new Font(Font.TIMES_ROMAN, H1);
	/**Times New Roman, 14 */
	public static final Font TIMES_NEW_ROMAN_H2 = new Font(Font.TIMES_ROMAN, H2);
	/**Times New Roman, 10 */
	public static final Font TIMES_NEW_ROMAN_H3 = new Font(Font.TIMES_ROMAN, H3);
	
	/**Courier, 8 */
	public static final Font COURIER_BODY = new Font(Font.COURIER, BODY);
	/**Courier, 8 BOLD */
	public static final Font COURIER_BOLD_BODY = new Font(Font.COURIER, BODY, Font.BOLD);
	/**Courier, 8 ITALIC */
	public static final Font COURIER_ITALIC_BODY = new Font(Font.COURIER, BODY, Font.ITALIC);
	/**Courier, 16 */
	public static final Font COURIER_H1 = new Font(Font.COURIER, H1);
	/**Courier, 14 */
	public static final Font COURIER_H2 = new Font(Font.COURIER, H2);
	/**Courier, 10 */
	public static final Font COURIER_H3 = new Font(Font.COURIER, H3);
	

}
